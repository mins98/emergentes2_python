paths = []
def menor_lista_1(lista):
    menor = lista[0]
    for valor in lista:
        if valor < menor:
            menor = valor
    return menor

def myDFS(graph,start,end,path): 
    path=path+[start]
   
    if start==end:
        paths.append(path)
        return paths
    if start in grafo.keys():
        nodos=grafo.get(start)
        dicAux={}
        for i in nodos:
            dicAux.setdefault(list(i.keys())[0],i.get(list(i.keys())[0]))
        valores=list(dicAux.values())
        menor=menor_lista_1(valores)
        llave=list(dicAux.keys())[list(dicAux.values()).index(menor)]
        myDFS(graph,llave,end,path)
    return paths



inicio='A'
buscado='G'

explorados=[]
grafo={'A':[{'B':70},{'C':35},{'E':55}],'B':[{'H':70}],'C' :[{'G':35}],'E':[{'F':55}],'F' :[{'I':55}],'H':[{'I':70}],'I':[{'G':72}]}
path=[inicio]
path= myDFS(grafo,inicio,buscado,[])
print (path)

         
